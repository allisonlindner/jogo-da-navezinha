//
//  Weapon.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 02/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit

class Weapon: NSObject {
	
	var bullet: Bullet!
	
	var type: NSInteger	= 0;
	var fireRate: Double = 1.0;
	
	init(type: NSInteger) {
		super.init();
		
		self.type = type;
		bullet = Bullet();
		
		switch(type) {
			
		case 0:
			fireRate = 0.5;
			break;
			
		default:
			break;
		}
	}
}
