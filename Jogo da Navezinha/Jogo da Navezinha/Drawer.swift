//
//  Drawer.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 02/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class Drawer: NSObject {
	
	class func DrawShip(ship: Ship) {
		
		switch(ship.type) {
			
		case 0:
			var bezierPath: UIBezierPath = UIBezierPath();
			
			bezierPath.moveToPoint		(CGPoint(	x: 0		, y: 0		));
			bezierPath.moveToPoint		(CGPoint(	x: 15		, y: 0		));
			bezierPath.addLineToPoint	(CGPoint(	x: 15		, y: 15		));
			bezierPath.addLineToPoint	(CGPoint(	x: -15		, y: 0		));
			bezierPath.addLineToPoint	(CGPoint(	x: 15		, y: -15	));
			bezierPath.addLineToPoint	(CGPoint(	x: 15		, y: 0		));
			bezierPath.moveToPoint		(CGPoint(	x: 0		, y: 0		));
			
			ship.sprite.path = bezierPath.CGPath;
			ship.sprite.lineWidth = 1.0;
			ship.sprite.strokeColor = UIColor.whiteColor();
			ship.sprite.fillColor = UIColor(red: 69.0/255, green: 117.0/255, blue: 133.0/255, alpha: 1.0);
			ship.sprite.antialiased = true;
			ship.sprite.physicsBody = SKPhysicsBody(polygonFromPath: bezierPath.CGPath);
			ship.sprite.physicsBody!.affectedByGravity = false;
			ship.sprite.physicsBody!.allowsRotation = false;
			ship.sprite.physicsBody!.restitution = 0.0;
			ship.sprite.physicsBody!.dynamic = true;
			
			ship.sprite.physicsBody!.categoryBitMask	=	BodyType.player.rawValue;
			ship.sprite.physicsBody!.collisionBitMask	=	BodyType.machineGunBullet.rawValue |
															BodyType.wall.rawValue;
			ship.sprite.physicsBody!.contactTestBitMask	=	BodyType.machineGunBullet.rawValue |
															BodyType.enemyShip.rawValue |
															BodyType.enemySpyShip.rawValue |
															BodyType.wall.rawValue;
			
			ship.sprite.position = CGPoint(x: CGFloat(ship.PosX), y: CGFloat(ship.PosY));
			break;
			
		case 1:
			var bezierPath: UIBezierPath = UIBezierPath();
			
			bezierPath.moveToPoint		(CGPoint(	x: 0.0		, y: 0.0	));
			bezierPath.moveToPoint		(CGPoint(	x: 80.0		, y: 100.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: 80.0		, y: -100.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: -80.0	, y: -100.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: -80.0	, y: 100.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: 80.0		, y: 100.0	));
			
			bezierPath.moveToPoint		(CGPoint(	x: 80.0		, y: 80.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: 100.0	, y: 80.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: 110.0	, y: -10.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: 110.0	, y: -100.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: 80.0		, y: -100.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: 80.0		, y: 80.0	));
			
			bezierPath.moveToPoint		(CGPoint(	x: -80.0	, y: 80.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: -100.0	, y: 80.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: -110.0	, y: -10.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: -110.0	, y: -100.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: -80.0	, y: -100.0	));
			bezierPath.addLineToPoint	(CGPoint(	x: -80.0	, y: 80.0	));
			
			bezierPath.moveToPoint		(CGPoint(	x: 0.0		, y: 0.0	));
			
			ship.sprite.path = bezierPath.CGPath;
			ship.sprite.lineWidth = 1.0;
			ship.sprite.strokeColor = UIColor.redColor();
			ship.sprite.fillColor = UIColor(red: 31.0/255, green: 31.0/255, blue: 31.0/255, alpha: 1.0);
			ship.sprite.antialiased = true;
			ship.sprite.physicsBody = SKPhysicsBody(polygonFromPath: bezierPath.CGPath);
			ship.sprite.physicsBody!.affectedByGravity = false;
			ship.sprite.physicsBody!.allowsRotation = false;
			ship.sprite.physicsBody!.restitution = 0.0;
			ship.sprite.physicsBody!.dynamic = false;
			
			ship.sprite.physicsBody!.categoryBitMask	= BodyType.enemyMotherShip.rawValue;
			ship.sprite.physicsBody!.contactTestBitMask = BodyType.machineGunBullet.rawValue;
			ship.sprite.physicsBody!.collisionBitMask	= BodyType.machineGunBullet.rawValue;
			
			ship.sprite.position = CGPoint(x: CGFloat(ship.PosX), y: CGFloat(ship.PosY));
			ship.sprite.runAction(SKAction.rotateByAngle(-0.785398163, duration: 0.5));
			break;
			
		case 2:
			var bezierPath: UIBezierPath = UIBezierPath();
			
			bezierPath.moveToPoint		(CGPoint(	x: -12		, y: 0		));
			bezierPath.addLineToPoint	(CGPoint(	x: -12		, y: 12		));
			bezierPath.addLineToPoint	(CGPoint(	x: 12		, y: 0		));
			bezierPath.addLineToPoint	(CGPoint(	x: -12		, y: -12	));
			bezierPath.addLineToPoint	(CGPoint(	x: -12		, y: 0		));
			bezierPath.moveToPoint		(CGPoint(	x: 0		, y: 0		));
			
			ship.sprite.path = bezierPath.CGPath;
			ship.sprite.lineWidth = 1.5;
			ship.sprite.strokeColor = UIColor.redColor();
			ship.sprite.fillColor = UIColor(red: 31.0/255, green: 31.0/255, blue: 31.0/255, alpha: 1.0);
			ship.sprite.antialiased = true;
			ship.sprite.physicsBody = SKPhysicsBody(polygonFromPath: bezierPath.CGPath);
			ship.sprite.physicsBody!.affectedByGravity = false;
			ship.sprite.physicsBody!.allowsRotation = false;
			ship.sprite.physicsBody!.restitution = 0.0;
			ship.sprite.physicsBody!.dynamic = true;
			
			ship.sprite.physicsBody!.categoryBitMask	=	BodyType.enemyShip.rawValue;
			ship.sprite.physicsBody!.contactTestBitMask =	BodyType.machineGunBullet.rawValue |
															BodyType.player.rawValue |
															BodyType.endOfScene.rawValue;
			ship.sprite.physicsBody!.collisionBitMask	=	BodyType.machineGunBullet.rawValue;
			
			ship.sprite.position = CGPoint(x: CGFloat(ship.PosX), y: CGFloat(ship.PosY));
			break;
			
		case 3:
			var bezierPath: UIBezierPath = UIBezierPath();
			
			bezierPath.moveToPoint		(CGPoint(	x: -12		, y: 0		));
			bezierPath.addLineToPoint	(CGPoint(	x: -12		, y: 12		));
			bezierPath.addLineToPoint	(CGPoint(	x: 12		, y: 0		));
			bezierPath.addLineToPoint	(CGPoint(	x: -12		, y: -12	));
			bezierPath.addLineToPoint	(CGPoint(	x: -12		, y: 0		));
			bezierPath.moveToPoint		(CGPoint(	x: 0		, y: 0		));
			
			ship.sprite.path = bezierPath.CGPath;
			ship.sprite.lineWidth = 1.5;
			ship.sprite.strokeColor = UIColor.blueColor();
			ship.sprite.fillColor = UIColor(red: 31.0/255, green: 31.0/255, blue: 31.0/255, alpha: 1.0);
			ship.sprite.alpha = 0.5;
			ship.sprite.antialiased = true;
			ship.sprite.physicsBody = SKPhysicsBody(polygonFromPath: bezierPath.CGPath);
			ship.sprite.physicsBody!.affectedByGravity = false;
			ship.sprite.physicsBody!.allowsRotation = false;
			ship.sprite.physicsBody!.restitution = 0.0;
			ship.sprite.physicsBody!.dynamic = true;
			
			ship.sprite.physicsBody!.categoryBitMask	=	BodyType.enemySpyShip.rawValue;
			ship.sprite.physicsBody!.contactTestBitMask =	BodyType.machineGunBullet.rawValue |
															BodyType.player.rawValue |
															BodyType.endOfScene.rawValue;
			ship.sprite.physicsBody!.collisionBitMask	=	BodyType.machineGunBullet.rawValue;
			
			ship.sprite.position = CGPoint(x: CGFloat(ship.PosX), y: CGFloat(ship.PosY));
			break;
			
		default:
			break;
		}
	}
	
	class func DrawBullet(ship: Ship) {
		
		switch(ship.machineGun.type) {
			
		case 0:
			var bullet = SKSpriteNode();
			ship.machineGun.bullet.sprite = SKSpriteNode();
			ship.machineGun.bullet.sprite.size = CGSize(width: 4, height: 2);
			ship.machineGun.bullet.sprite.color = UIColor(red: 255.0/255, green: 153.0/255, blue: 0.0/255, alpha: 1.0);
			ship.machineGun.bullet.sprite.physicsBody = SKPhysicsBody(rectangleOfSize: ship.machineGun.bullet.sprite.size);
			
			ship.machineGun.bullet.sprite.physicsBody!.dynamic = true;
			ship.machineGun.bullet.sprite.physicsBody!.allowsRotation = false;
			ship.machineGun.bullet.sprite.physicsBody!.affectedByGravity = false;
			
			ship.machineGun.bullet.sprite.physicsBody!.categoryBitMask		=	BodyType.machineGunBullet.rawValue;
			ship.machineGun.bullet.sprite.physicsBody!.contactTestBitMask	=	BodyType.enemyMotherShip.rawValue |
																				BodyType.enemyShip.rawValue |
																				BodyType.enemySpyShip.rawValue |
																				BodyType.wall.rawValue;
			ship.machineGun.bullet.sprite.physicsBody!.collisionBitMask		=	BodyType.enemyMotherShip.rawValue |
																				BodyType.enemyShip.rawValue |
																				BodyType.enemySpyShip.rawValue |
																				BodyType.wall.rawValue;
			
			ship.machineGun.bullet.sprite.position = CGPoint(x: CGFloat(ship.sprite.position.x - 20), y: CGFloat(ship.sprite.position.y));
			break;
			
		default:
			break;
		}
	}
}
