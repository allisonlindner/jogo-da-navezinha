//
//  PlayerShip.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 02/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class PlayerShip: Ship {
	
	var name: NSString = "";
	var defaultFireRate = 0.08;
	
	init(PosX: Float, PosY: Float) {
		super.init();
		
		name = "Player";
		
		engine = 0.0;
		propulsion = 2.0;
		type = 0;
		
		machineGun.bullet.speed = -0.2;
		machineGun.fireRate = defaultFireRate;
		
		self.PosX = PosX - 110.0;
		self.PosY = PosY;
	}
}
