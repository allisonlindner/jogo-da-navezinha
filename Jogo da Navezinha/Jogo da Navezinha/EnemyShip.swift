//
//  EnemyShip.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 04/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit

class EnemyShip: Ship {
	
	override init() {
		super.init();
		
		type = 2;
		engine = 120.0;
	}
	
}
