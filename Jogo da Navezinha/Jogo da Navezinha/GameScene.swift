//
//  GameScene.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 01/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import SpriteKit

enum BodyType: UInt32 {
	
	case player = 1
	case enemyShip = 2
	case enemyMotherShip = 4
	case enemySpyShip = 8
	case machineGunBullet = 16
	case wall = 32
	case endOfScene = 64
}

class GameScene: SKScene, SKPhysicsContactDelegate {
	
	var particles: SKEmitterNode!
	var stars: SKEmitterNode!
	var player: PlayerShip!
	var enemyMotherShip: EnemyMotherShip = EnemyMotherShip();
	
	var stunnerTimeHUD: SKLabelNode!
	
	var spawner1 = CGPoint(x: -80.0, y: 200);
	var spawner2 = CGPoint(x: -60.0, y: 250);
	var spawner3 = CGPoint(x: -40.0, y: 300);
	var spawner4 = CGPoint(x: -60.0, y: 350);
	var spawner5 = CGPoint(x: -60.0, y: 80);
	
	var waveNumber = 1;
	var enemyKilledOnThisWave = 0;
	var enemyKilledTotal = 0;
	var enemyShipsOnScreen = 0;
	var arrayOfEnemys = NSMutableArray(capacity: 0);
	var arrayOfSpyEnemys = NSMutableArray(capacity: 0);
	
	var timeSinceLastUpdate = 0.0;
	var lastUpdateTime = 0.0;
	var timeForShoot = 0.0;
	var timeForSpawn = 0.0;
	var spySpawned = false;
	
	var timeStuned = 0.0;
	
	var playerShooting = false;
	
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
		
		self.physicsWorld.contactDelegate = self;
		
		backgroundColor = UIColor.blackColor();
		
		stars = SKEmitterNode(fileNamed: "stars");
		stars.position = CGPoint(x: 0, y: view.bounds.size.height);
		stars.hidden = false;
		
		self.addChild(stars);
		
		player = PlayerShip(PosX: Float(view.bounds.size.width), PosY: Float(view.center.y));
		Drawer.DrawShip(player);
		Drawer.DrawShip(enemyMotherShip);
		
		addChild(player.sprite);
		addChild(enemyMotherShip.sprite);
		
		stunnerTimeHUD = SKLabelNode();
		stunnerTimeHUD.fontSize = 20;
		stunnerTimeHUD.position = CGPoint(x: scene!.view!.bounds.width/2, y: 10);
		stunnerTimeHUD.fontColor = UIColor.whiteColor();
		stunnerTimeHUD.hidden = true;
		addChild(stunnerTimeHUD);
    }
	
	func didBeginContact(contact: SKPhysicsContact) {
		
		let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask;
		
		switch(contactMask) {
			
		case BodyType.player.rawValue | BodyType.enemyShip.rawValue:
			
			timeStuned = 2.0;
			player.machineGun.fireRate = 0.0;
			break;
		
		case BodyType.player.rawValue | BodyType.enemySpyShip.rawValue:
			
			timeStuned = 3.0;
			player.machineGun.fireRate = 0.0;
			break;
			
		case BodyType.machineGunBullet.rawValue | BodyType.enemyMotherShip.rawValue:
			
			if(contact.bodyA.categoryBitMask == BodyType.machineGunBullet.rawValue) {
				if(contact.bodyA != nil) {
					if(contact.bodyA.node != nil) {
						addExplosion(contact.bodyA.node!, explosionFilePath: "machineGunBulletExplosion");
						contact.bodyA.node!.removeFromParent();
					}
				}
			} else if(contact.bodyB.categoryBitMask == BodyType.machineGunBullet.rawValue) {
				if(contact.bodyB != nil) {
					if(contact.bodyB.node != nil) {
						addExplosion(contact.bodyB.node!, explosionFilePath: "machineGunBulletExplosion");
						contact.bodyB.node!.removeFromParent();
					}
				}
			}
			
			println("Colidiu com a Nave Mãe");
			
			break;
		
		case BodyType.machineGunBullet.rawValue | BodyType.enemyShip.rawValue:
			
			if(contact.bodyA.categoryBitMask == BodyType.enemyShip.rawValue) {
				if(contact.bodyA != nil) {
					if(contact.bodyA.node != nil) {
						addExplosion(contact.bodyA.node!, explosionFilePath: "enemyShipExplosion");
						contact.bodyA.node!.removeFromParent();
						arrayOfEnemys.removeObject(contact.bodyA.node!);
					}
				}
			} else if(contact.bodyA.categoryBitMask == BodyType.machineGunBullet.rawValue) {
				if(contact.bodyA != nil) {
					if(contact.bodyA.node != nil) {
						addExplosion(contact.bodyA.node!, explosionFilePath: "machineGunBulletExplosion");
						contact.bodyA.node!.removeFromParent();
					}
				}
			}
			
			if(contact.bodyB.categoryBitMask == BodyType.enemyShip.rawValue) {
				if(contact.bodyB != nil) {
					if(contact.bodyB.node != nil) {
						addExplosion(contact.bodyB.node!, explosionFilePath: "enemyShipExplosion");
						contact.bodyB.node!.removeFromParent();
						arrayOfEnemys.removeObject(contact.bodyB.node!);
					}
				}
			} else if(contact.bodyB.categoryBitMask == BodyType.machineGunBullet.rawValue) {
				if(contact.bodyB != nil) {
					if(contact.bodyB.node != nil) {
						addExplosion(contact.bodyB.node!, explosionFilePath: "machineGunBulletExplosion");
						contact.bodyB.node!.removeFromParent();
					}
				}
			}
			
			println("Enemy Ship killed");
			
			break;
			
		case BodyType.machineGunBullet.rawValue | BodyType.enemySpyShip.rawValue:
			
			if(contact.bodyA.categoryBitMask == BodyType.enemySpyShip.rawValue) {
				if(contact.bodyA != nil) {
					if(contact.bodyA.node != nil) {
						addExplosion(contact.bodyA.node!, explosionFilePath: "enemySpyShipExplosion");
						contact.bodyA.node!.removeFromParent();
						arrayOfSpyEnemys.removeObject(contact.bodyA.node!);
					}
				}
			} else if(contact.bodyA.categoryBitMask == BodyType.machineGunBullet.rawValue) {
				if(contact.bodyA != nil) {
					if(contact.bodyA.node != nil) {
						addExplosion(contact.bodyA.node!, explosionFilePath: "machineGunBulletExplosion");
						contact.bodyA.node!.removeFromParent();
					}
				}
			}
			
			if(contact.bodyB.categoryBitMask == BodyType.enemySpyShip.rawValue) {
				if(contact.bodyB != nil) {
					if(contact.bodyB.node != nil) {
						addExplosion(contact.bodyB.node!, explosionFilePath: "enemySpyShipExplosion");
						contact.bodyB.node!.removeFromParent();
						arrayOfSpyEnemys.removeObject(contact.bodyB.node!);
					}
				}
			} else if(contact.bodyB.categoryBitMask == BodyType.machineGunBullet.rawValue) {
				if(contact.bodyB != nil) {
					if(contact.bodyB.node != nil) {
						addExplosion(contact.bodyB.node!, explosionFilePath: "machineGunBulletExplosion");
						contact.bodyB.node!.removeFromParent();
					}
				}
			}
			
			println("Enemy Spy Ship killed");
			
			break;
			
		case BodyType.machineGunBullet.rawValue | BodyType.wall.rawValue:
			
			if(contact.bodyA.categoryBitMask == BodyType.machineGunBullet.rawValue) {
				if(contact.bodyA != nil) {
					if(contact.bodyA.node != nil) {
						contact.bodyA.node?.removeFromParent();
					}
				}
			} else {
				if(contact.bodyB != nil) {
					if(contact.bodyB.node != nil) {
						contact.bodyB.node?.removeFromParent();
					}
				}
			}
			
			println("Colidiu com a parede");
			
			break;
			
		default:
			break;
		}
	}
	
	func didEndContact(contact: SKPhysicsContact) {
		
		let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask;
		
		switch(contactMask) {
			
		case BodyType.enemyShip.rawValue | BodyType.endOfScene.rawValue:
			if(contact.bodyA.categoryBitMask == BodyType.enemyShip.rawValue) {
				if(contact.bodyA != nil) {
					if(contact.bodyA.node != nil) {
						contact.bodyA.node?.removeFromParent();
						arrayOfEnemys.removeObject(contact.bodyA.node!);
					}
				}
			} else {
				if(contact.bodyB != nil) {
					if(contact.bodyB.node != nil) {
						contact.bodyB.node?.removeFromParent();
						arrayOfEnemys.removeObject(contact.bodyB.node!);
					}
				}
			}
			println("Enemy Ship removida");
			break;
			
		case BodyType.enemySpyShip.rawValue | BodyType.endOfScene.rawValue:
			if(contact.bodyA.categoryBitMask == BodyType.enemyShip.rawValue) {
				if(contact.bodyA != nil) {
					if(contact.bodyA.node != nil) {
						contact.bodyA.node?.removeFromParent();
						arrayOfSpyEnemys.removeObject(contact.bodyA.node!);
					}
				}
			} else {
				if(contact.bodyB != nil) {
					if(contact.bodyB.node != nil) {
						contact.bodyB.node?.removeFromParent();
						arrayOfSpyEnemys.removeObject(contact.bodyB.node!);
					}
				}
			}
			println("Enemy Ship removida");
			break;
			
		default:
			break;
		}
		
	}
	
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
		
		timeSinceLastUpdate = currentTime - lastUpdateTime;
		lastUpdateTime = currentTime;
		timeForSpawn += timeSinceLastUpdate;
		
		if(timeStuned > 0) {
			var timerLabel = NSString(format: "Stunned: %.1f", timeStuned);
			
			timeStuned -= timeSinceLastUpdate;
			
			stunnerTimeHUD.text = timerLabel;
			stunnerTimeHUD.hidden = false;
			
			if(timeStuned <= 3.0 && timeStuned > 2.5) {
				player.sprite.fillColor = UIColor.redColor();
			} else if(timeStuned <= 2.5 && timeStuned > 2.1) {
				player.sprite.fillColor = UIColor(red: 69.0/255, green: 117.0/255, blue: 133.0/255, alpha: 1.0);
			} else if(timeStuned <= 2.1 && timeStuned > 1.6) {
				player.sprite.fillColor = UIColor.redColor();
			} else if(timeStuned <= 1.6 && timeStuned > 1.2) {
				player.sprite.fillColor = UIColor(red: 69.0/255, green: 117.0/255, blue: 133.0/255, alpha: 1.0);
			} else if(timeStuned <= 1.2 && timeStuned > 0.8) {
				player.sprite.fillColor = UIColor.redColor();
			} else if(timeStuned <= 0.8 && timeStuned > 0.5) {
				player.sprite.fillColor = UIColor(red: 69.0/255, green: 117.0/255, blue: 133.0/255, alpha: 1.0);
			} else if(timeStuned <= 0.5 && timeStuned > 0.2) {
				player.sprite.fillColor = UIColor.redColor();
			} else if(timeStuned <= 0.2 && timeStuned > 0.1) {
				player.sprite.fillColor = UIColor(red: 69.0/255, green: 117.0/255, blue: 133.0/255, alpha: 1.0);
			} else if(timeStuned <= 0.1 && timeStuned > 0.0) {
				player.sprite.fillColor = UIColor.redColor();
			}
		} else {
			player.machineGun.fireRate = player.defaultFireRate;
			player.sprite.fillColor = UIColor(red: 69.0/255, green: 117.0/255, blue: 133.0/255, alpha: 1.0);
			timeStuned = 0.0;
			stunnerTimeHUD.hidden = true;
		}
		
		if(playerShooting) {
			timeForShoot += timeSinceLastUpdate;
			
			if(timeForShoot >= player.machineGun.fireRate && player.machineGun.fireRate > 0) {
				Drawer.DrawBullet(player);
				addChild(player.machineGun.bullet.sprite);
				player.machineGun.bullet.sprite.physicsBody?
					.applyImpulse(CGVector(	dx: CGFloat(player.machineGun.bullet.speed)	,
											dy:					0.0						));
				timeForShoot = 0.0;
			}
		} else {
			timeForShoot = 0.0;
		}
		
		if(timeForSpawn >= 5 && !spySpawned) {
			if(arc4random()%50 == 5) {
				spawnSpyEnemys();
				spySpawned = true;
			}
		}
		
		if(timeForSpawn >= 7) {
			
			spawnEnemys();
			
			timeForSpawn = 0.0;
			spySpawned = false;
			
			if(waveNumber < 10) {
				waveNumber++;
			}
		}
		
		for(var i = 0; i < arrayOfEnemys.count ; i++) {
			var enemy: EnemyShip = EnemyShip();
			enemy.sprite = arrayOfEnemys.objectAtIndex(i) as SKShapeNode;
			enemy.sprite.physicsBody!.velocity =
										CGVector(dx: CGFloat(enemy.engine), dy: 0);
		}
		
		for(var i = 0; i < arrayOfSpyEnemys.count ; i++) {
			var enemy: EnemySpyShip = EnemySpyShip();
			enemy.sprite = arrayOfSpyEnemys.objectAtIndex(i) as SKShapeNode;
			enemy.sprite.physicsBody!.velocity =
				CGVector(dx: CGFloat(enemy.engine), dy: 0);
		}
    }
	
	func addExplosion(node: SKNode, explosionFilePath: NSString) {
		
		particles = SKEmitterNode(fileNamed: explosionFilePath);
		particles.position = node.position;
		particles.hidden = false;
		
		self.addChild(particles);
		node.hidden = true;
	}
	
	func spawnEnemys() {
		
		enemyShipsOnScreen = 0;
		var currentRow = 0;
		
		while(enemyShipsOnScreen < waveNumber*4) {
			var enemy = EnemyShip();
			
			switch(enemyShipsOnScreen%4) {
				
			case 0:
				enemy.PosX = Float(Float(spawner1.x) - (Float(currentRow) * 50));
//				enemy.PosX = Float(spawner1.x);
				enemy.PosY = Float(spawner1.y);
				break;
				
			case 1:
				enemy.PosX = Float(Float(spawner2.x) - (Float(currentRow) * 50));
//				enemy.PosX = Float(spawner2.x);
				enemy.PosY = Float(spawner2.y);
				break;
				
			case 2:
				enemy.PosX = Float(Float(spawner3.x) - (Float(currentRow) * 50));
//				enemy.PosX = Float(spawner3.x);
				enemy.PosY = Float(spawner3.y);
				break;
				
			case 3:
				enemy.PosX = Float(Float(spawner4.x) - (Float(currentRow) * 50));
//				enemy.PosX = Float(spawner4.x);
				enemy.PosY = Float(spawner4.y);
				break;
				
			default:
				break;
			}
			
			Drawer.DrawShip(enemy);
			addChild(enemy.sprite);
			enemy.sprite.physicsBody!.velocity = CGVector(dx: CGFloat(enemy.engine), dy: 0);
			arrayOfEnemys.addObject(enemy.sprite);
			enemyShipsOnScreen++;
			
			if(enemyShipsOnScreen%4 == 0) {
				currentRow++;
			}
		}
	}
	
	func spawnSpyEnemys() {
		var numberOfSpyShips = 0;
		while(numberOfSpyShips < 4) {
			var enemy = EnemySpyShip();
			
			enemy.PosX = Float(Float(spawner5.x) - (Float(numberOfSpyShips) * 50));
			enemy.PosY = Float(spawner5.y);
			
			Drawer.DrawShip(enemy);
			addChild(enemy.sprite);
			enemy.sprite.physicsBody!.velocity = CGVector(dx: CGFloat(enemy.engine), dy: 0);
			arrayOfSpyEnemys.addObject(enemy.sprite);
			numberOfSpyShips++;
		}
	}

}
